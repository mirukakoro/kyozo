def var_check(var_name, var_mold, var_input, err_msg_tpl='Variable {var_name} doesn\'t match {var_type}', err_type=RuntimeError):
  if type(var_mold) == type(list()) or type(var_mold) == type(tuple()):
    err_count = 0
    err_msg_mold = ''
    for var_mold_i in var_mold: # I really coulgn't think of a better name for var_mold_i...
      err_msg_mold += '{}, '.format(type(var_mold_i))
      if type(var_mold_i) != type(var_input):
        err_count += 1
    if err_count >= len(var_mold):
      raise err_type(err_msg_tpl.format(var_name=var_name, var_type=err_msg_mold))
  else:
    if type(var_mold) != type(var_input):
      raise err_type(err_msg_tpl.format(var_name=var_name, var_type=type(var_mold)))