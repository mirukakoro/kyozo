import bare
import checker


class base(bare.bare):
  def __init__(self, var_check=True, verbose=True, verbose_report='{} \t {} \t {}', set_scene=False, scene_path='models/environment', scene_scale=0.25, scene_pos=(-8, 42, 0)): # TODO: Change verbose to verbosity (add support for multiple levels of verbosity instead of two (0, 1), eg 0, 1, 2)
    if verbose: print(verbose_report.format('Msg', 'NA', 'Kyozo Wrapper base.py'))
    self.var_check = var_check
    if self.var_check: checker.var_check('scene_path', str(), scene_path)
    if self.var_check: checker.var_check('scene_scale', [float(), int(), tuple(), list()], scene_scale)
    if self.var_check: checker.var_check('scene_pos', [list(), tuple()], scene_pos)
    if self.var_check: checker.var_check('set_scene', bool(), set_scene)
    if self.var_check: checker.var_check('verbose', bool(), verbose)
    if self.var_check: checker.var_check('verbose_report', str(), verbose_report)
    if verbose: print(verbose_report.format('Done', 'OK', 'Var check'))
    bare.bare.__init__(self)
    if verbose: print(verbose_report.format('Done', 'OK', 'Super class __init__'))
    # Def vars
    self.set_scene = set_scene
    if verbose: print(verbose_report.format('Msg', 'NA', 'set_scene is {}'.format(self.set_scene)))
    if self.set_scene:
      # Def vars
      self.scene_path = scene_path
      if verbose: print(verbose_report.format('Done', 'OK', 'Def vars'))
      # Load scene
      self.scene      = self.loader.loadModel(scene_path)
      if verbose: print(verbose_report.format('Done', 'OK', 'Load scene'))
      # Render scene
      self.scene.reparentTo(self.render)
      if verbose: print(verbose_report.format('Done', 'OK', 'Reparent to render'))
      # Set scene scale
      if verbose: print(verbose_report.format('Msg', 'NA', 'Scene scale input type may not appear for unknown reasons'))
      if type(scene_scale) == type(int()):
        if verbose: print(verbose_report.format('Doing', 'OK', 'Scene scale input is list/tuple'))
        self.scene.setScale(scene_scale, scene_scale, scene_scale)
      elif type(scene_scale) == type(list()) or type(scene_scale) == type(tuple()):
        if verbose: print(verbose_report.format('Doing', 'OK', 'Scene scale input is float/int'))
        self.scene.setScale(*scene_scale)
      if verbose: print(verbose_report.format('Done', 'OK', 'Set scene scale'))
      # Set scene pos
      self.scene.setPos(*scene_pos)
      if verbose: print(verbose_report.format('Done', 'OK', 'Set scene pos'))
    if verbose: print(verbose_report.format('Msg', 'NA', 'Controling the Camera: Mouse 0: L/R; Mouse 1: Rotate (origin); Mouse 2: F/B; Mouse 1+2: Rotate (camera)'))

  def add_task(self, task, name):
    self.taskMgr.add(task, name)

  def set_cam_pos(self, pos):
    if self.var_check: checker.var_check('pos', [tuple(), list()], pos)
    self.camera.setPos(*pos)
  
  def set_cam_hpr(self, hpr):
    if self.var_check: checker.var_check('hpr', [tuple(), list()], hpr)
    self.camera.setPos(*hpr)

if __name__ == '__main__':
  app = base(verbose=True)
  app.run()