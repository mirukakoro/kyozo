import bullet
import interactive
from math import pi, sin, cos

class test(bullet.bullet):#, interactive.interactive):
  def __init__(self, **kwargs):
    bullet.bullet.__init__(self, **kwargs)
    # interactive.interactive.__init__(self, **kwargs)

if __name__ == '__main__':
  print('hello')
  app = test()
  app.add_bullet_plane()
  # n = input()
  n = 10000
  if n == '':
    n = 5
  else:
    n = int(n)
  import random
  for i in range(0, n*1, 1):
    app.add_bullet_box(pos=(random.randint(-10, 10), random.randint(-10, 10), i))
  app.run()